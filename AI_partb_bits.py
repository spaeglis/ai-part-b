def recursive_move_finder(state_array, depth, max_depth):
    if(depth == max_depth):
        return evaluation_fn(stat_array[depth])
    moves = state_array[depth].get_moves(board.colours((depth-1)%3))
    if(depth==1):
        expected_eval = -100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval > expected_eval):
                expected_eval = new_eval
                best_move = move
            array_undo_move(state_array, depth, move)
        return best_move
    if((depth-1)%3==0):
        expected_eval = -100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval > expected_eval):
                expected_eval = new_eval
            array_undo_move(state_array, depth, move)
        return expected_eval
    else:
        expected_eval = 100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval < expected_eval):
                expected_eval = new_eval
            array_undo_move(state_array, depth, move)
        return expected_eval

def array_apply_move(state_array, depth, move):
    for i in range(len(state_array)-depth):
        state_array[depth+i].move(move)

def array_undo_move(state_array, depth, move):
    for i in range(len(state_array)-depth):
        state_array[depth+i].undo_move(move)
