from Snek.util import Board
import pickle

class Player:

	def __init__(self, colour):
		self.colour = colour
		#self.board = Board.Board(self.colour)
		states = []
		for i in range(6):
			states.append(Board.Board(self.colour))

	def on_board(self, piece, move):
		if piece.pos[0] + move[0] < -3 or piece.pos[0] + move[0] > 3:
			return False

		elif piece.pos[1] + move[1] < -3 or piece.pos[1] + move[1] > 3:
			return False

		return True

	def action(self):
		"""
		moves = self.board.get_moves(self.colour)
		moves[0] = self.board.convert_coords_ref(moves[0])
		print(moves[0])
		return(moves[0])
		"""



	def update(self, colour, action):
		action = self.board.convert_coords_local(action)
		self..move(action)

	def recursive_move_finder(self, state_array, depth, max_depth):
	    if(depth == max_depth):
	        return evaluation_fn(stat_array[depth], Board.colours[(depth-1)%3])
	    moves = state_array[depth].get_moves(board.colours[(depth-1)%3])
	    if(depth==1):
	        expected_eval = -100000
	        for move in moves:
	            array_apply_move(state_array, depth, move)
	            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
	            if(new_eval > expected_eval):
	                expected_eval = new_eval
	                best_move = move
	            array_undo_move(state_array, depth, move, board.colours[(depth-1)%3])
	        return best_move
	    if((depth-1)%3==0):
	        expected_eval = -100000
	        for move in moves:
	            array_apply_move(state_array, depth, move)
	            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
	            if(new_eval > expected_eval):
	                expected_eval = new_eval
	            array_undo_move(state_array, depth, move, board.colours[(depth-1)%3])
	        return expected_eval
	    else:
	        expected_eval = 100000
	        for move in moves:
	            array_apply_move(state_array, depth, move)
	            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
	            if(new_eval < expected_eval):
	                expected_eval = new_eval
	            array_undo_move(state_array, depth, move, board.colours[(depth-1)%3])
	        return expected_eval

	def array_apply_move(self, state_array, depth, move):
	    for i in range(len(state_array)-depth):
	        state_array[depth+i].move(move)

	def array_undo_move(self, state_array, depth, move):
	    for i in range(len(state_array)-depth):
	        state_array[depth+i].undo_move(move)

	def evaluation_fn(self, state):
		evl = 0
		for piece in state.pieces[self.board.colour]:
			evl += ((min_dist(piece)-position_value(node, piece))/2 + 1)
		return evl

	def min_dist(self, piece):
		min_dist = 10
	    for tile in self.board.exit_tiles[piece.colour]:
	        if self.board.distance(piece.pos, tile) <= min_dist:
	            min_dist = board.distance(piece.pos, tile)
	    return min_dist

	# Calculates a "score" for a position based on the number of available jumps
	# that get you closer to the goal state
	def position_value(node, piece):
	    adjacent_piece = pickle.loads(pickle.dumps(piece))
	    value = 0
	    if piece.pos in self.board.exit_tiles[piece.colour]:
	        return value
	    for move in node.moves:
	        adjacent_piece.pos = (piece.pos[0] + move[0], piece.pos[1] + move[1])
	        adjacent_min = min_dist(adjacent_piece)
	        current_min = min_dist(piece)
	        if checkJump(node, piece.pos, move) and on_board(adjacent_piece.pos):
	            if adjacent_min < current_min:
	                value += 0.5
	            if adjacent_min == current_min:
	                value += 0.25

	            if adjacent_min == current_min+1:
	                value += 0.15
	        if (
	                on_board(adjacent_piece.pos) and
	                type(node.tiles[adjacent_piece.pos[1]+3][adjacent_piece.pos[0]+3]) == Piece.Piece
	            ):
	            value += 0.1
	    return value

	# Check if a jump is possible from a position in a given direction
	def checkJump(node, position, move):
	    new = (position[0] + move[0], position[1] + move[1])
	    new_jump = (position[0] + 2*move[0], position[1] + 2*move[1])
	    if on_board(new) and on_board(new_jump):
	        if (
	                type(board.tiles[new[0]+3][new[1]+3]) == Piece.Piece and 
	                type(board.tiles[new_jump[0]+3][new_jump[1]+3]) == None
	            ):
	            return True
	        else:
	            return False

	# Check if a position is on the board
	def on_board(position):
	    if position[0] >= -3 and position[0] <= 3 and position[1] >= -3 and position[1] <= 3:
	        return True
	    else:
	        return False
