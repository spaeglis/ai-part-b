from Snek.util import Piece

class Board:
	# List of move directions
	directions = [(-1,0), (0,-1), (1,-1), (1,0), (0,1), (-1,1)]

	def __init__(self, colour):

		self.colour = colour

		self.colours = []

		if self.colour == 'red':
			self.colours = ['red', 'green', 'blue']

		elif self.colour == 'green':
			self.colours = ['green', 'blue', 'red']

		else:
			self.colours = ['blue', 'red', 'green']

		# List of exit tiles for each colour
		self.exit_tiles = {'red':[(6,0), (6,1), (6,2), (6,3)], 'green':[(0,6),(1,6),(2,6),(3,6)],'blue':[(3,0),(2,1),(1,2),(0,3)]}

		self.tiles = [[None for i in range(7)] for j in range(7)]
		self.tiles[0][0] = 0
		self.tiles[0][1] = 0
		self.tiles[0][2] = 0
		self.tiles[1][0] = 0
		self.tiles[1][1] = 0
		self.tiles[2][0] = 0
		self.tiles[4][6] = 0
		self.tiles[5][5] = 0
		self.tiles[5][6] = 0
		self.tiles[6][4] = 0
		self.tiles[6][5] = 0
		self.tiles[6][6] = 0

		# List of pieces on the board
		self.pieces = {'red':[], 'green':[], 'blue':[]}
		
		for i in range(3,7):
			self.pieces['red'].append(Piece.Piece((0,i),"red"))

		for i in range(3,7):
			self.pieces['green'].append(Piece.Piece((i,0),"green"))

		j = 6
		for i in range(3,7):
			self.pieces['blue'].append(Piece.Piece((i,j),"blue"))
			j = j - 1

		for piece in self.pieces['red']:
			self.tiles[piece.pos[1]][piece.pos[0]] = piece

		for piece in self.pieces['green']:
			self.tiles[piece.pos[1]][piece.pos[0]] = piece

		for piece in self.pieces['blue']:
			self.tiles[piece.pos[1]][piece.pos[0]] = piece

		j = 6
		for i in range(3,7):
			print(self.tiles[i][0])
			print(self.tiles[0][i])
			print(self.tiles[j][i])
			j = j - 1

		#print self.tiles


	def get_moves(self, colour):
		valid_moves = []
		for piece in self.pieces[colour]:

			# If the piece is on an exit tile, return that piece
			if piece.pos in self.exit_tiles[piece.colour]:
				valid_moves.append(('EXIT', piece.pos))
			p_r = piece.pos[1]
			p_q = piece.pos[0]

			# Otherwise, try all moves and return valid ones
			for direction in self.directions:
				if not self.on_board(piece, direction):
					continue

				if self.tiles[p_r + direction[1]][p_q + direction[0]] is None:
					valid_moves.append(('MOVE',((p_q, p_r),(p_q + direction[0], p_r + direction[1]))))
					continue

				direction = (direction[0]*2, direction[1]*2)
				if self.on_board(piece, direction):
					if self.tiles[p_r + direction[1]][p_q + direction[0]] is None:
						valid_moves.append(('JUMP',((p_q, p_r),(p_q + direction[0], p_r + direction[1]))))
		
		return valid_moves

	def on_board(self, piece, direction):
		if piece.pos[0] + direction[0] < 0 or piece.pos[0] + direction[0] > 6:
			return False

		elif piece.pos[1] + direction[1] < 0 or piece.pos[1] + direction[1] > 6:
			return False

		return True

	def convert_coords_local(self, action):
		if action[0] == 'EXIT':
			return (action[0], (action[1][0]+3, action[1][1]+3))

		return (action[0], ((action[1][0][0]+3, action[1][0][1]+3), (action[1][1][0]+3, action[1][1][1]+3)))

	def convert_coords_ref(self, action):
		if action[0] == 'EXIT':
			return (action[0], (action[1][0]-3, action[1][1]-3))

		return (action[0], ((action[1][0][0]-3, action[1][0][1]-3), (action[1][1][0]-3, action[1][1][1]-3)))

	def distance(self, start, end):
        q1 = start[0]
        q2 = end[0]
        r1 = start[1]
        r2 = end[1]
        return (abs(q1 - q2) + abs(q1 + r1 - q2 - r2) + abs(r1 - r2))/2

	def move(self, action):

		m_from = action[1][0]
		m_to = action[1][1]

		if action[0] ==  'MOVE':
			piece = self.tiles[action[1][0][1]][action[1][0][0]]
			self.tiles[action[1][0][1]][action[1][0][0]] = None
			self.tiles[action[1][1][1]][action[1][1][0]] = piece
			self.tiles[action[1][1][1]][action[1][1][0]].pos = action[1][1]

		elif action[0] == 'JUMP':
			piece = self.tiles[action[1][0][1]][action[1][0][0]]
			direction = (action[1][1][0]-action[1][0][0], action[1][1][1]-action[1][0][1])
			direction = (int(direction[0]/2), int(direction[1]/2))
			mid_tile = (piece.pos[0] + direction[0], piece.pos[1] + direction[1])
			taken_piece = self.tiles[mid_tile[1]][mid_tile[0]]
			print(len(self.pieces[taken_piece.colour]))
			for i in range(len(self.pieces[taken_piece.colour])):
				if self.pieces[taken_piece.colour][i] is taken_piece:
					self.pieces[taken_piece.colour].pop(i)
					self.pieces[piece.colour].append(taken_piece)
					taken_piece.colour = piece.colour
					break
			self.tiles[action[1][0][1]][action[1][0][0]] = None
			self.tiles[action[1][1][1]][action[1][1][0]] = piece
			self.tiles[action[1][1][1]][action[1][1][0]].pos = action[1][1]


		elif action[0] == 'EXIT':
			piece = self.tiles[action[1][1]][action[1][0]]
			self.tiles[action[1][1]][action[1][0]] = None
			self.pieces[piece.colour].remove(piece)


	def undo_move(self, move, colour):

        move_from = move[1][1]
        move_to = move[1][0]

		if move[0] ==  'MOVE':
			shift_piece(move_from, move_to)

		elif move[0] == 'JUMP':
			piece = self.tiles[move_from[1]][move_from[0]]
			direction = move_direction(move_from,move_to)
			mid_tile = (piece.pos[0] + direction[0], piece.pos[1] + direction[1])
			capture_piece(mid_tile, self.tiles[mid_tile[1]][mid_tile[0]])
			shift_piece(move_from, move_to)

		elif move[0] == 'EXIT':
			piece = Piece.Piece(move[1], colour)
			self.tiles[move[1][1]][move[1][0]] = piece
			self.pieces[piece.colour].append(piece)

	def capture_piece(self, pos, colour):
	    taken_piece = self.tiles[pos[1]][pos[0]]
	    for i in range(len(self.pieces[taken_piece.colour])):
	        if self.pieces[taken_piece.colour][i] is taken_piece:
	            self.pieces[taken_piece.colour].pop(i)
	            self.pieces[colour].append(taken_piece)
	            taken_piece.colour = colour
	            break

	def shift_piece(self, t_from, t_to):
	    piece = self.tiles[t_from[1]][t_from[0]]
	    self.tiles[t_from[1]][t_from[0]] = None
	    self.tiles[t_to[1]][t_to[0]] = piece
	    piece.pos = t_to

	def move_direction(move_from, move_to):
	    direction = (move_to[0]-move_from[0], move_to[1]-move_from[1])
	    return (int(direction[0]/2), int(direction[1]/2))
