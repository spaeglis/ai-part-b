def on_board(self, q, r):
    if q >= 0 and q <= 6 and r >= 0 and r <= 6:
        return True
    else:
        return False



def getMoves(self, colour):
    valid_moves = []
    for piece in self.pieces[colour]:

        q = piece.pos[0]
        r = piece.pos[1]

        # Otherwise, try all moves and return valid ones
        for direction in self.directions:
            q_moved = q + direction[0]
            r_moved = r + direction[1]
            if(on_board(q_moved, r_moved) && self.tiles[q_moved][r_moved] is None):
                valid_moves.append(('MOVE',(q, r),(q_moved, r_moved)))
            else
                q_jumped = q_moved + direction[0]
                r_jumped = r_moved + direction[1]
                if(on_board(q_jumped, r_jumped) && self.tiles[q_jumped][r_jumped] is None):
                    valid_moves.append(('JUMP',(q, r),(q_jumped, r_jumped)))

        if piece.pos in self.exit_tiles[colour]:
            valid_moves.append(('EXIT',(q, r)))

    if len(valid_moves) == 0:
        valid_moves.append(('PASS',))

    return valid_moves




def recursive_move_finder(state_array, depth, max_depth):
    if(depth == max_depth):
        return evaluation_fn(stat_array[depth])
    moves = state_array[depth].get_moves(board.colours((depth-1)%3))
    if(depth==1):
        expected_eval = -100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval > expected_eval):
                expected_eval = new_eval
                best_move = move
            array_undo_move(state_array, depth, move)
        return best_move
    if((depth-1)%3==0):
        expected_eval = -100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval > expected_eval):
                expected_eval = new_eval
            array_undo_move(state_array, depth, move)
        return expected_eval
    else:
        expected_eval = 100000
        for move in moves:
            array_apply_move(state_array, depth, move)
            new_eval = recursive_move_finder( state_array, depth+1, max_depth)
            if(new_eval < expected_eval):
                expected_eval = new_eval
            array_undo_move(state_array, depth, move)
        return expected_eval

def array_apply_move(state_array, depth, move):
    for i in range(len(state_array)-depth):
        state_array[depth+i].move(move)

def array_undo_move(state_array, depth, move):
    for i in range(len(state_array)-depth):
        state_array[depth+i].undo_move(move)

def move(self, move):

        move_from = move[1][0]
        move_to = move[1][1]

		if move[0] ==  'MOVE':
			shift_piece(move_from, move_to)

		elif move[0] == 'JUMP':
			piece = self.tiles[move_from[1]][move_from[0]]
			direction = move_direction(move_from,move_to)
			mid_tile = (piece.pos[0] + direction[0], piece.pos[1] + direction[1])
			capture_piece(mid_tile, piece.colour)
			shift_piece(move_from, move_to)

		elif move[0] == 'EXIT':
			piece = self.tiles[move[1][1]][move[1][0]]
			self.tiles[move[1][1]][move[1][0]] = None
			self.pieces[piece.colour].remove(piece)

def undo_move(self, move, colour):

        move_from = move[1][1]
        back_to = move[1][0]

		if move[0] ==  'MOVE':
			shift_piece(move_from, move_to)

		elif move[0] == 'JUMP':
			piece = self.tiles[move_from[1]][move_from[0]]
			direction = move_direction(move_from,move_to)
			mid_tile = (piece.pos[0] + direction[0], piece.pos[1] + direction[1])
			capture_piece(mid_tile, self.tiles[mid_tile[1]][mid_tile[0]])
			shift_piece(move_from, move_to)

		elif move[0] == 'EXIT':
			piece = Piece.Piece(move[1], colour)
			self.tiles[move[1][1]][move[1][0]] = piece
			self.pieces[piece.colour].append(piece)

def capture_piece(self, pos, colour):
    taken_piece = self.tiles[pos[1]][pos[0]]
    for i in range(len(self.pieces[taken_piece.colour])):
        if self.pieces[taken_piece.colour][i] is taken_piece:
            self.pieces[taken_piece.colour].pop(i)
            self.pieces[colour].append(taken_piece)
            taken_piece.colour = colour
            break

def shift_piece(self, t_from, t_to):
    piece = self.tiles[t_from[1]][t_from[0]]
    self.tiles[t_from[1]][t_from[0]] = None
    self.tiles[t_to[1]][t_to[0]] = piece
    piece.pos = t_to

def move_direction(move_from, move_to):
    direction = (move_to[0]-move_from[0], move_to[1]-move_from[1])
    return (int(direction[0]/2), int(direction[1]/2))
