from itertools import cycle
from numpy.random import randint,choice
import sys
import neat
import os
import random

GENERATION = 0
MAX_FITNESS = 0
BEST_GENOME = 0

def eval_genomes(genomes, config):
	i = 0
	global SCORE
	global GENERATION, MAX_FITNESS, BEST_GENOME

	GENERATION += 1
	for genome_id, genome in genomes:
		
		genome.fitness = random.randint(1,10)
		print("Gen : %d Genome # : %d  Fitness : %f Max Fitness : %f"%(GENERATION,i,genome.fitness, MAX_FITNESS))
		if genome.fitness >= MAX_FITNESS:
			MAX_FITNESS = genome.fitness
			BEST_GENOME = genome
		SCORE = 0
		i+=1


config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         'config')

pop = neat.Population(config)
stats = neat.StatisticsReporter()
pop.add_reporter(stats)

winner = pop.run(eval_genomes, 30)

print(winner)